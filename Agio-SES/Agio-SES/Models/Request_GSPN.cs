﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Agio_SES.Models
{
    [Table("request_GSPN")]
    public class request_GSPN
    {
        [Key]

        public int ID { get; set; }
        public string request_no { get; set; }
        public string ticket { get; set; }
        public Nullable<int> ID_solicitud { get; set; }
        public string localizacion_2 { get; set; }
        public string localizacion_3 { get; set; }
        public string comentarios { get; set; }

    }
}