﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Web;
using System.Web.Mvc;
using Agio_SES.Models;

namespace Agio_SES.Controllers
{
    public class RequestController : Controller
    {
        // GET: Request
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public ActionResult ShowRequest()
        {
            return View();
        }

        public ActionResult LoadRequest()
        {
            try
            {
                //Creating instance of DatabaseContext class
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var draw = Request.Form.GetValues("draw").FirstOrDefault(); //dibuja secuencia de devoluciones de forma asincrona
                    var start = Request.Form.GetValues("start").FirstOrDefault(); //inicio en el conjunto de datos actual(0 basado en índice, es decir, 0 es el primer registro).
                    var length = Request.Form.GetValues("length").FirstOrDefault();// Número de registros que la tabla puede mostrar
                    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
                    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
                    var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


                    //Paging Size (10,20,50,100)  
                    int pageSize = length != null ? Convert.ToInt32(length) : 0;
                    int skip = start != null ? Convert.ToInt32(start) : 0;
                    int recordsTotal = 0;

                    // obteniendo todos los datos de la orden 
                    var requestData = (from tempID in _context.request_GSPN
                                     select tempID);

                    //Sorting(clasificación)
                    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
                    {
                        requestData = requestData.OrderBy(sortColumn + " " + sortColumnDir);
                    }

                    ////Search  buscar
                    if (!string.IsNullOrEmpty(searchValue))
                    {
                        requestData = requestData.Where(m => m.request_no == searchValue
                        || m.ticket == searchValue || m.localizacion_2 == searchValue);
                    }

                    //total number of rows count   
                    recordsTotal = requestData.Count();
                    //Paging   
                    var data = requestData.Skip(skip).Take(pageSize).ToList();

                    return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data }, JsonRequestBehavior.AllowGet);

                }
            }

            catch (Exception)
            {

                throw;
            }

        }

        [HttpGet]
        public ActionResult EditRequest(int? ID)
        {
            try
            {
                using (DatabaseContext _context = new DatabaseContext())
                {
                    var request = (from request_GSPN in _context.request_GSPN
                                 where request_GSPN.ID == ID
                                 select request_GSPN).FirstOrDefault();

                    return View(request);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }


        //[HttpPost]
        //public JsonResult DeleteOrden(string ID)
        //{
        //    using (DatabaseContext _context = new DatabaseContext())
        //    {
        //        var Orden = _context.sc_OrderStatus.Find(ID);

        //        if (ID == null)
        //            return Json(data: "Not Deleted", behavior: JsonRequestBehavior.AllowGet);
        //        _context.sc_OrderStatus.Remove(Orden);
        //        _context.SaveChanges();

        //        return Json(data: "Deleted", behavior: JsonRequestBehavior.AllowGet);
        //    }
        //}

    }
}